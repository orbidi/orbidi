import requests
import json
from sqlalchemy import create_engine
import pandas as pd
import pymysql


# Definir el ID de la lista y el token de API clientes
list = [900100953291,900100953296,900100953297]

for ele in list:
    id = ele
    print(id)
    api_token = "pk_3041236_DXOBGO4W2M220TSFKLKVFUDZ50WP4VG9"

    # Definir la URL de la API con el ID de la lista
    url = f"https://api.clickup.com/api/v2/list/{id}/task"

    # Configurar los headers de la solicitud con el token de API
    headers = {
        "Authorization": api_token
    }

    # Hacer la solicitud GET a la API con los headers
    response = requests.get(url, headers=headers)

    # Comprobar si la solicitud fue exitosa (código de estado 200)
    if response.status_code == 200:
        # Obtener los datos de la respuesta en formato JSON
        data = response.json()
        # Procesar los datos o realizar operaciones necesarias
        data_string = json.dumps(data, indent=4)
        #print(data_string)
    else:
        # Mostrar un mensaje de error en caso de que la solicitud haya fallado
        print("Error en la solicitud: ", response.status_code)


    df = pd.json_normalize(data["tasks"], record_path="linked_tasks", meta=["name","id", "url", "team_id", "time_estimate"])
    df[["producto", "name2"]] = df["name"].str.split(": ", expand=True)

    # Strip leading and trailing whitespace
    df["producto"] = df["producto"].str.strip()
    df["name2"] = df["name2"].str.strip()

    proyect_mapping = {
        "KD-WEB": 0,
        "KD-SEO": 1,
        "KD-ANALITICA": 2,
        "KD-RRSS": 3,
        "KD-ECOMMERCE": 4,
        "KD-CRM": 5,
        "KD-PROC": 6,
        "KD-FACT": 7
    }

    df["producto"] = df["producto"].map(proyect_mapping)

    df["time_estimate"] = df["time_estimate"].astype(float)

    #sumatoria_por_producto = df.groupby("proyect")["time_estimate"].sum()
    #print(sumatoria_por_producto)
    #print(df)

    engine = create_engine('mysql+mysqlconnector://guio:guio123**@localhost:3306/orbidi')
    df.to_sql(con=engine, name='proyectos', if_exists='append', index=False)
    engine.dispose()