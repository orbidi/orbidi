import requests
import json
from sqlalchemy import create_engine
import pandas as pd
import pymysql


# Definir el ID de la lista y el token de API clientes
#list = [900100953154,900100953291,900100953296,900100953297]

id = 900100953154
api_token = "pk_3041236_DXOBGO4W2M220TSFKLKVFUDZ50WP4VG9"

# Definir la URL de la API con el ID de la lista
url = f"https://api.clickup.com/api/v2/list/{id}/task"

# Configurar los headers de la solicitud con el token de API
headers = {
    "Authorization": api_token
}

# Hacer la solicitud GET a la API con los headers
response = requests.get(url, headers=headers)

# Comprobar si la solicitud fue exitosa (código de estado 200)
if response.status_code == 200:
    # Obtener los datos de la respuesta en formato JSON
    data = response.json()
    # Procesar los datos o realizar operaciones necesarias
    data_string = json.dumps(data, indent=4)
    print(data_string)
else:
    # Mostrar un mensaje de error en caso de que la solicitud haya fallado
    print("Error en la solicitud: ", response.status_code)

import pandas as pd
df = pd.json_normalize(data["tasks"], record_path="linked_tasks", meta=["name", "id", "url", "team_id"])

# Mostrar el DataFrame
print(df)

engine = create_engine('mysql+mysqlconnector://guio:guio123**@localhost:3306/orbidi')
df.to_sql(con=engine, name='clientes', if_exists='append', index=False)
engine.dispose()