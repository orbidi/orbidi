from sqlalchemy import create_engine

# Crear una conexión a la base de datos MySQL
engine = create_engine('mysql+mysqlconnector://guio:guio123**@localhost:3306')

# Crear una base de datos
engine.execute("CREATE DATABASE orbidi")  # Reemplaza "mydatabase" con el nombre de la base de datos que deseas crear

print("Base de datos creada exitosamente!")